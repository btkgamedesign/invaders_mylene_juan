﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StateManager : MonoBehaviour
{
	public GameObject hud;
	public GameObject highScores;

	State currentState;

	enum State 
	{
      START,
	  INGAME,
	  END
	}

	public static StateManager Instance;
	public Settings settings;
	int lives;
	public GameObject playerPrefab;

	void Start()
	{
		Instance = this;
		SwitchState(State.START);
	}

	void SwitchState(State s)
	{
		currentState = s;

		switch (s)
		{
			case State.START:
				//
				SwitchState(State.INGAME);
				break;
			case State.INGAME:
				Time.timeScale = 1;
				lives = settings.lives;
				SpawnPlayer();
				hud.SetActive(true);
				highScores.SetActive(false);
				break;
			case State.END:
				Time.timeScale = 0;
				hud.SetActive(false);
				highScores.SetActive(true);
				HighScoresManager.Instance.EnterNewScore(Score.score);
				HighScoresManager.Instance.PopulateScoreTable();
				break;
		}
	}

	public void LostLife()
	{
		lives--;
		if (lives > 0)
		{
			SpawnPlayer();
		}
		else
		{
			Debug.Log("Game Over, all player cannons have been destroyed");
			Loss();
		}
	}

	void SpawnPlayer()
	{
		Instantiate(playerPrefab);
	}

	public void Loss()
	{
		SwitchState(State.END);
		Score.Instance.ResetScore();
	}

	public void Victory()
	{
		Debug.Log("Victory!");
		SwitchState(State.END);
	}

	public void RestartGame()
	{
		SceneManager.LoadScene("Main");
	}

	public void RestartButton()
	{
		SceneManager.LoadScene("Main");
		Score.score = 0;
	}

	public void QuitGame()
	{
		Application.Quit();
	}

}
