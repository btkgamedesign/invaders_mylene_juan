﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectPool : MonoBehaviour
{
    public static EffectPool Instance;
    public GameObject effectPrefab;
    public int howManyEffects;

    List<GameObject> effects;

    void Start()
    {
        Instance = this;

        effects = new List<GameObject>();
        int effectId = 0;

        for (int e = 0; e < howManyEffects; e++)
        {
            GameObject effect = Instantiate(effectPrefab, transform);
            effect.SetActive(false);
            effect.name = effect.name = $"effect {effectId}";
            effectId++;
            effects.Add(effect);
        }
    }

    public ParticleSystem GetEffect()
    {
        GameObject e = effects[0];
        effects.RemoveAt(0);
        ParticleSystem ps = e.GetComponent<ParticleSystem>();
        ps.Clear();
        e.SetActive(true);
        return ps;
    }

    public void ReturnEffect(GameObject o)
    {
        o.SetActive(false);
        effects.Add(o);
    }
}
