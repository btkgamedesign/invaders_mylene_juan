﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Settings" , order = 1)]
public class Settings : ScriptableObject
{
    public float spacing;
    public float speedFactor;
    public float invaderMoveRange;
    public float invaderSpeedFactor;
    public float bulletSpeed;
    public float invaderShotChancePerSecond;
    public float invaderSpeedMultiplierFactor;
    public int lives;

    public int startingInvaders;
    public int lines;
    public float bonusAppearChancePerSecond;
    public float bonusSpeedMultiplier;
}
