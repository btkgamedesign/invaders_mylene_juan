﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float leftLimit;
    public float rightLimit;
    public Settings settings;

    void Update()
    {

        // move player right
        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            transform.Translate(new Vector3(settings.speedFactor * Time.deltaTime, 0, 0));
        }

        // move player left
        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            transform.Translate(new Vector3(-settings.speedFactor * Time.deltaTime, 0, 0));
        }

        // shoot a bullet
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Bullet b = BulletPool.instance.GetBullet();
            b.transform.position = transform.position + Vector3.up;
            b.Shoot(gameObject, Vector3.up);

        }

        // keep player in bounds
        float newX = Mathf.Clamp(transform.position.x, leftLimit, rightLimit);
        if (transform.position.x != newX)
        {
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }

        
    }

    void OnTriggerEnter(Collider other)
    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        if (b != null)
        {
            // intersected with a bullet
            gameObject.SetActive(false);
            b.Reset();

            gameObject.GetComponentInParent<AudioSource>().Play();

            //game over?
            StateManager.Instance.LostLife();
        }
        else
        {
            Invader i = other.gameObject.GetComponent<Invader>();
            if (i != null)
            {
                gameObject.GetComponentInParent<AudioSource>().Play();
                Debug.Log("Player cannon destroyed by collision with invaders");
                StateManager.Instance.Loss();
            }
        }
    }
}
