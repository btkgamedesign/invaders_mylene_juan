﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class InvadersManager : MonoBehaviour
{
    public Settings settings;
    
    public GameObject invaderPrefab;
    public GameObject bonusPrefab;
    List<GameObject> invaders;

    public List<GameObject> invaderGraphics;
    static GameObject activeBonusInvader;

    void Start()
    {
        invaders = new List<GameObject>();
        int invadersPerLine = settings.startingInvaders / settings.lines;

        for ( int x = 0; x < invadersPerLine; x++)
        {
            for (int y = 0; y < settings.lines; y++) 
            {
                GameObject invader = Instantiate(invaderPrefab, transform);
                invaders.Add(invader);

                Instantiate(invaderGraphics[y], invader.transform);


                invader.transform.localPosition = new Vector3(
                    settings.spacing * (x - invadersPerLine/2f + 0.5f), settings.spacing * -y, 0);

                invader.name = $"<{x}, {y}> invader";
            }
        }
    }

    void Update()
    {
        if (Random.value < settings.bonusAppearChancePerSecond * Time.deltaTime && activeBonusInvader == null)
        {
           activeBonusInvader = Instantiate(bonusPrefab);
        }
    }

}
