﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunkerCube : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        if (b != null)
        {
            // intersected with a bullet
            gameObject.SetActive(false);
            b.Reset();
        }

        else 
        {
            Invader i = other.gameObject.GetComponent<Invader>();
            // intersected with an invader
            if (i != null)
            {
                gameObject.SetActive(false);
            }
             
        }
    }
}
