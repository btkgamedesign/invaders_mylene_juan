﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public bool isFloor;
    void OnTriggerEnter(Collider other)
    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        if (b != null)
        {
            // intersected with a bullet
            b.Reset();
        }

        else if (isFloor)
        {
            Invader i = other.gameObject.GetComponent<Invader>();
            if (i != null) 
            {
                Debug.Log("game over! the invaders have reach earth");
                StateManager.Instance.Loss();
            }
        }
    }
}
