﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bonus : MonoBehaviour
{
    public Settings settings;
    float moveX = 0;
    public float leftLimit;
    void Update()
    {

        float speedMultiplier = settings.bonusSpeedMultiplier + settings.invaderSpeedMultiplierFactor * Invader.killed / settings.startingInvaders;
        moveX = speedMultiplier * -settings.invaderSpeedFactor * Time.deltaTime;
        transform.Translate(moveX, 0, 0);

        if(transform.position.x < leftLimit)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        // check we collided with a bullet that's not shot by us
        if (b != null && b.shooter != gameObject)
        {
           
            Score.Instance.AddToScore(10);

            // intersected with a bullet 
            Destroy(this.gameObject);
            b.Reset();


            // play effect
            ParticleSystem e = EffectPool.Instance.GetEffect();
            e.transform.position = transform.position;
            e.Play();
            e.GetComponentInParent<AudioSource>().Play();

            //return after playing
            DOVirtual.DelayedCall(1, () =>
            {
                // Cleanup
                EffectPool.Instance.ReturnEffect(e.gameObject);

            }
            );
        }
    }
}
