﻿using UnityEngine;
using DG.Tweening;

public class Invader : MonoBehaviour
{
   public static int killed;

    public Settings settings;
    float leftLimit;
    float rightLimit;
    bool goingRight;
    Vector3 initialPosition;

    void Start()
    {
        initialPosition = transform.position;

        goingRight = true;
    }



    void Flip()
    {
        goingRight = !goingRight;
        //go down
        transform.Translate(0, -.5f, 0);
    }

    void Update()
    {
        float speedMultiplier = 1 + settings.invaderSpeedMultiplierFactor * killed / settings.startingInvaders;

        leftLimit = initialPosition.x - settings.invaderMoveRange;
        rightLimit = initialPosition.x + settings.invaderMoveRange;

        //movement
        float moveX = 0;


        if (goingRight)
        {
            //right movement
            if (transform.position.x > rightLimit)
            {
                Flip();
            }
            else
            {
                moveX = speedMultiplier * settings.invaderSpeedFactor * Time.deltaTime;
            }
        }
        else
        {
            //left movement
            if (transform.position.x < leftLimit)
            {
                Flip();
            }
            else
            {
                moveX = speedMultiplier * -settings.invaderSpeedFactor * Time.deltaTime;
            }
        }

        transform.Translate(moveX, 0, 0);


        //shoot ranodmly
        if (Random.value < settings.invaderShotChancePerSecond * Time.deltaTime)
        {
            int layerMask = 1 << LayerMask.NameToLayer("Invader");
            bool hasInvaderUnderMe = Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), 10, layerMask);
            if (!hasInvaderUnderMe)
            {
                //only if there are no invaders below me
                Bullet b = BulletPool.instance.GetBullet();
                b.transform.position = transform.position + Vector3.down;
                b.Shoot(gameObject, Vector3.down);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        // check we collided with a bullet that's not shot by us
        if (b != null && b.shooter.tag != "Invader")
        {
            killed++;
            Score.Instance.AddToScore(1);

            // intersected with a bullet 
            gameObject.SetActive(false);
            b.Reset();


            // play effect
            ParticleSystem e = EffectPool.Instance.GetEffect();
            e.transform.position = transform.position;
            e.Play();
            e.GetComponentInParent<AudioSource>().Play();


            //return after playing
            DOVirtual.DelayedCall(1, () =>
            {
                // Cleanup
                EffectPool.Instance.ReturnEffect(e.gameObject);

                // Check for victory
                if(killed >= settings.startingInvaders)
                {
                    killed = 0;
                    StateManager.Instance.Victory();
                }
            }
            );
        }
    }
}
