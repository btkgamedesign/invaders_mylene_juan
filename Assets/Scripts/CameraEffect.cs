﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraEffect : MonoBehaviour
{
    public void Shake()
    {
        transform.DOShakePosition(.25f, .25f);
    }
}