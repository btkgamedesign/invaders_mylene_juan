﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FallIntoPlace : MonoBehaviour
{
    void Start()
    {
        float targetY = transform.position.y;
        transform.Translate(Vector3.up * 10);
        transform.DOMoveY(targetY, Random.Range(0.5f, 1)).SetEase(Ease.OutBounce);
    }
}
