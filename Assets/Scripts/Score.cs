﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
	public static Score Instance;
	public static int score;

	Text label;

	void Start()
	{
		Instance = this;
		label = GetComponent<Text>();
		UpdateLabel();
	}

	void UpdateLabel()
	{
		label.text = $" <color=white>Score: {score}</color>";
	}

	public void ResetScore()
	{
		score = 0;
		UpdateLabel();
	}

	public void AddToScore(int toAdd)
	{
		score += toAdd;
		UpdateLabel();
	}
}